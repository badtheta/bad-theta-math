package btMath;

public class Complex {

	private double real;
	private double imaginary;
	
	public Complex(){
		real=0;
		imaginary=1;
	}
	public Complex(double r, double i){
		real = r;
		imaginary = i;
	}
	public Complex(Complex copy){
		real = copy.getReal();
		imaginary = copy.getImaginary();
	}
	
	public void setReal(double r){real = r;}
	public void setImaginary(double i){imaginary = i;}
	
	public double getReal(){return real;}
	public double getImaginary(){return imaginary;}
	
	public String toString(){
		return real+" + "+imaginary+"i";
	}
	
}//end complex
