package btMath;



public class Vector2D extends Vector3D{

	private float[] vals = new float[3];
	private int quad;
	

	/**************Constructors****************/
	public Vector2D(){
		vals[0]=0;
		vals[1]=0;
		vals[2]=1;
	}//Origin Constructor
	
	public Vector2D(float x, float y){
		vals[0]=x;
		vals[1]=y;
		vals[2]=1;
		findQuad();
	}//float Constructor
	
	public Vector2D(double x, double y){
		vals[0]=(float)x;
		vals[1]=(float)y;
		vals[2]=1;
		findQuad();
	}//double Constructor
	
	public Vector2D(int x, int y){
		vals[0]=x;
		vals[1]=y;
		vals[2]=1;
		findQuad();
	}//int Constructor
	
	public Vector2D(Vector2D pt){
		vals[0]=pt.getX();
		vals[1]=pt.getY();
		vals[2]=1;
		quad=pt.getQuad();
	}
	
	public Vector2D(float mag, double dir){
		vals[0]=(float) (mag*Math.cos(dir));
		vals[1]=(float) (mag*Math.sin(dir));
		vals[2]=1;
		Round();
		findQuad();
	}
	
	
	/******************Accessors***************************/
	public float getX(){return vals[0];}
	public float getY(){return vals[1];}
	public float getMag(){return (float) Math.sqrt( (vals[0]*vals[0]) + (vals[1]*vals[1]) );}
	public int getQuad(){return quad;}
	public double getDir(){
		if(quad==0 || quad==2)
			return  Math.atan2(Math.abs(vals[1]),Math.abs(vals[0])) + (quad*(LinMath.PI/2)); 
		else
			return  ((LinMath.PI/2)-(Math.atan2(Math.abs(vals[1]),Math.abs(vals[0]))) + (quad*(LinMath.PI/2))); 
	}
	public float[] getVals(){return vals;}
	/**********************Mutators*******************************/
	public void chgX(float x){vals[0]=x;}
	public void chgY(float y){vals[1]=y;}
	public void chgX(int x){vals[0]=x;}
	public void chgY(int y){vals[1]=y;}
	public void chgX(double x){vals[0]=(float)x;}
	public void chgY(double y){vals[1]=(float)y; }
	public void multX(float s){vals[0]*=s;}	
	public void multX(int s){vals[0]*=s;}
	public void multX(double s){vals[0]*=s;}
	public void multY(float s){vals[1]*=s;}	
	public void multY(int s){vals[1]*=s;}
	public void multY(double s){vals[1]*=s;}
	public void addX(float s){vals[0]+=s;}	
	public void addX(int s){vals[0]+=s;}
	public void addX(double s){vals[0]+=s;}
	public void addY(float s){vals[1]+=s;}	
	public void addY(int s){vals[1]+=s;}
	public void addY(double s){vals[1]+=s;}
	


	public void findQuad(){
		if( vals[0]==0 ){
			if(vals[1]>=0)
				quad=1;
			else
				quad=3;
		}
		else if( vals[0]>0 ){
			if(vals[1]>=0)
				quad=0;
			else
				quad=3;
		}
		else{
			if(vals[1]>0)
				quad=1;
			else 
				quad=2;

		}
	}//end findQuad
	
	public String toString(){
		return "<"+vals[0]+","+vals[1]+"> Dir: "+getDir()+"  Quadrant: "+(quad+1);
	}
	
	public void Round(){
		
		if( Math.abs(((int)(vals[0]) - vals[0])) <= .001 )
			vals[0]= (int)(vals[0]);
		if( Math.abs(((int)(vals[1]) - vals[1])) <= .001 )
			vals[1]= (int)(vals[1]);
		
	}
	
	
}//end point2D
