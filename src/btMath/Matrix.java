package btMath;

/***
 * @author James Birchfield
 *
 *Bad Theta
 */

public class Matrix {

	private float[][] elements;
	private int col;
	private int row;
	
	public Matrix(){
		elements = new float[0][0];
		elements[0][0]=0;
		col=0;
		row=0;
	}
	
	public Matrix(int c, int r){
		elements= new float[r][c];
		row=r;
		col=c;
		reset();
	}
	
	public Matrix(float[] ele, int r, int c){
		elements= new float[r][c];
		row=r;
		col=c;
		int place=0;
		for(int x=0; x<r; x++){
			for(int y=0; y<c; y++){
				if(place<ele.length)
					elements[x][y]=ele[place];
				else
					elements[x][y]=0;
				place++;
			}
		}
		
		
	}
	
	public void reset(){
		for(int x=0; x<row; x++){
			for(int y=0; y<col; y++){
					elements[x][y]=0;
			}
		}
	}
	
	/********************Accesors*************************/
	public int getNumRow(){return row;}
	public int getNumCol(){return col;}
	public float[][] getElements(){
		float[][] ret = new float[row][col];
		
		for(int x=0; x<col; x++){
			for(int y=0; y<row; y++){
				ret[x][y]=elements[x][y];
			}
		}
		
		return ret;
	}//end getElements()
	public float getElement(int r,int c){return elements[r][c];}
	/************************Mutators*****************************/
	public void chgCol(int c){col=c;}
	public void chgRow(int r){row=r;}
	public void chgElement(int r,int c,float val){elements[r][c]=val;}
	public void chgAllElements(float[][] vals){
		for(int x=0; x<col; x++){
			for(int y=0; y<row; y++){
				elements[x][y]=vals[x][y];
			}
		}
	}
	
	public String toString(){
		String ret = "[";
		
		for(int x=0; x<row; x++){
			for(int y=0; y<col; y++){
					ret+=" "+elements[x][y];
			}
			ret+=" ]\n[";
		}
		return ret;
	}//end toString
	
	
	
}//end Matrix
