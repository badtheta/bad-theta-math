package btMath;

/**
 * 
 * 			This class is meant to be a replacement for
 * 	 	matrix style rotations by representing rotations
 * 		with an angle, in radians, and an axis to rotate
 * 		around.
 * 
 * @author James H. Birchfield IV
 * @since 1.1
 */
 
public class Quaternion {
	public final static Quaternion AddIdentity = new Quaternion(0.0 ,0.0 ,0.0 ,0.0 );
	public final static Quaternion MultIdentity = new Quaternion(1.0 ,0.0 ,0.0 ,0.0 );
	
   /***Float array representing Quaternion*
    * quat[0] = w
    * quat[1] = x
    * quat[2] = y
    * quat[3] = z
    * */
	private double[] quat = new double[4];
	
	/**
	 * Constructor
	 * @param wp = cos( Angle/2 )
	 * @param xp = x * sin( Angle/2 )
	 * @param yp = y * sin( Angle/2 )
	 * @param zp = z * sin( Angle/2 )
	 */
	public Quaternion(float wp, float xp, float yp, float zp){
		quat[0] = wp;
		quat[1] = xp;
		quat[2] = yp;
		quat[3] = zp;
	}
	/**
	 * Constructor
	 * @param wp = cos( Angle/2 )
	 * @param xp = x * sin( Angle/2 )
	 * @param yp = y * sin( Angle/2 )
	 * @param zp = z * sin( Angle/2 )
	 */
	public Quaternion(double wp, double xp, double yp, double zp){
		quat[0] = wp;
		quat[1] = xp;
		quat[2] = yp;
		quat[3] = zp;
	}
	/**
	 * Constructor
	 * @param angle Rotation around an axis in radians
	 * @param x X component of the rotation axis
	 * @param y Y component of the rotation axis
	 * @param z Z component of the rotation axis
	 */
	public Quaternion(double angle, float x, float y, float z){
		quat[0] = (float) (Math.cos(angle/2));
		quat[1] = x * (float) (Math.sin(angle/2));
		quat[2] = y * (float) (Math.sin(angle/2));
		quat[3] = z * (float) (Math.sin(angle/2));
	}
	/**
	 * Constructor
	 * @param angle Angle to rotate around the rotation axis in radians
	 * @param axis The axis to rotate around
	 */
	public Quaternion(double angle, Vector3D axis){
		quat[0] = (Math.cos(angle/2));
		quat[1] = axis.getX() * (Math.sin(angle/2));
		quat[2] = axis.getY() * (Math.sin(angle/2));
		quat[3] = axis.getZ() * (Math.sin(angle/2));
	}
	/**
	 * Copy Constructor
	 * @param q Quaternion to copy
	 */
	public Quaternion(Quaternion q){
		quat[0] = q.getW();
		quat[1] = q.getX();
		quat[2] = q.getY();
		quat[3] = q.getZ();
	}
	
	
	public double getW(){return quat[0];}
	public double getX(){return quat[1];}
	public double getY(){return quat[2];}
	public double getZ(){return quat[3];}
	
	public String toString(){
		return "<"+quat[0]+" , "+quat[1]+" , "+quat[2]+" , "+quat[3]+">";
	}
	
}//end Quaternion
