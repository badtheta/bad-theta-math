package btMath;

import static java.lang.System.out;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LinMath math = new LinMath();
		
		Vector3D up = new Vector3D(0,1,0);
		Vector3D right = new Vector3D(0,0,1);
		Vector3D forward = new Vector3D(1,0,0);
		
		//Quaternion rotUP = new Quaternion(math.PI/20, right);
		Quaternion rotRight = new Quaternion(math.PI/10, up);
		Quaternion rot = new Quaternion(
				//math.Quattimes(rotUP, rotRight)
				rotRight);
		
		out.println("Forward: "+forward);
		out.println("Up: "+up);
		out.println("Right: "+right+"\n");

		for(int i=0; i<10; i++){
			up = math.Quatrotate(rot, up);
			forward = math.Quatrotate(rot, forward);
			right = math.Quatrotate(rot, right);
		}
		
		out.println("Forward: "+forward);
		out.println("Up: "+up);
		out.println("Right: "+right+"\n");
		
		
		up = math.findOrthog(forward, up);
		right = math.findOrthog(forward, right);
		right = math.findOrthog(up, right);
		forward = math.unitvec(forward);
		up = math.unitvec(up);
		right = math.unitvec(right);
		
		out.println("Forward: "+forward);
		out.println("Up: "+up);
		out.println("Right: "+right);

	}

}
